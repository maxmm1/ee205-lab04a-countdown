///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Max Mochizuki <maxmm@hawaii.edu>
// @date   2 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

int main( int argc, char* argv[] ) {

// Defining a Reference time to count up or down from
struct tm referenceTime = {0};
   referenceTime.tm_year = 99;
   referenceTime.tm_mon = 5;
   referenceTime.tm_mday = 13;
   referenceTime.tm_hour = 8;
   referenceTime.tm_min = 30;
   referenceTime.tm_sec = 15;

// Struct for local time
time_t currentTime;
struct tm *localTime;

// Get and print reference time
mktime ( &referenceTime );
printf("\nReference Time is %s\n", asctime( &referenceTime ) );

// Loop and countdown or countup every second
while (1) {
   
   // Get local time
   time( &currentTime );
   localTime = localtime( &currentTime );
   mktime ( localTime );

   // Get difference in time in seconds
   double diffTime = fabs( difftime( currentTime, mktime( &referenceTime )));
   int year = floor( diffTime / 31536000 );
   int day  = floor( fmod( ( diffTime / 86400 ), 365 ));
   int hour = floor( fmod( ( diffTime / 3600 ), 24 ));
   int min  = floor( fmod( ( diffTime / 60 ), 60 ));
   int sec  = floor( fmod( diffTime, 60 ));

   // Print countdown or countup
   printf( "Years: %d   ", year );
   printf( "Days: %d   ", day );
   printf( "Hours: %d   ", hour );
   printf( "Minutes: %d   ", min );
   printf( "Seconds: %d\n", sec );

   // Delay for 1 second before repeating loop
   sleep( 1 );
}

   return 0;
}
